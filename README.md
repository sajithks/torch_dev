# README #



This is a development version of Torch. The main branch of Torch do not support spatially weighted cross-entropy loss/criterion.
Here both C and CUDA versions of spatially weighted cross-entropy criterion is implemented. It is implemented as class negative log likelihood criterion.


### Installation ###

Copy the entire content to torch/extra folder. Then run the normal torch installation script.


#### For CPU implementation ####

require 'torch'
require 'nn'
require 'optim'
require 'cunn'
require 'cudnn'
require 'nngraph'
require 'cutorch'

input = nn.Identity()()   
layer1 = cudnn.SpatialConvolution(1, 2, 3, 3, 1, 1, 1, 1)(input)     
layer2 = nn.SpatialLogSoftMax()(layer1)           
gmod = nn.gModule({input}, {layer2})               

#### For GPU implementation ####

input = nn.Identity(input = nn.Identity()()               
layer1 = cudnn.SpatialConvolution(1, 2, 3, 3, 1, 1, 1, 1)(input)             
gmod = nn.gModule({input}, {layer1})                
criterion = cudnn.SpatialCrossEntropyCriterionWeighted()      







require 'nn'

local SpatialCrossEntropyCriterionWeighted, parent = torch.class('cudnn.SpatialCrossEntropyCriterionWeighted', 'nn.Criterion')

--[[
    This criterion does the SpatialCrossEntropyCriterion across
    the feature dimension for a N-channel image of HxW in size.

    It only supports mini-batches (4D input, 3D target)

    It does a LogSoftMax on the input (over the channel dimension),
    so no LogSoftMax is needed in the network at the end

    input = batchSize x nClasses x H x W
    target = batchSize x H x W
]]--
function SpatialCrossEntropyCriterionWeighted:__init(weights)
    parent.__init(self)
    self.slsm = cudnn.SpatialLogSoftMax()
    self.nllw = nn.SpatialClassNLLCriterionWeighted()
    self.sizeAverage = true
end

function SpatialCrossEntropyCriterionWeighted:updateOutput(input, target, weights)
    assert(input:dim() == 4, 'mini-batch supported only')
    assert(target:dim() == 3, 'mini-batch supported only')
    assert(input:size(1) == target:size(1), 'input and target should be of same size')
    assert(input:size(3) == target:size(2), 'input and target should be of same size')
    assert(input:size(4) == target:size(3), 'input and target should be of same size')
    assert(weights:size(2) == target:size(2), 'weights and target should be of same size')
    assert(weights:size(3) == target:size(3), 'weights and target should be of same size')
    -- apply SpatialLogSoftMax to input
    -- print('call slsm ')
    self.slsm:updateOutput(input)
    -- print('call slsm done ')

    -- Update submodule sizeAverage to make it consistent.
    self.nllw.sizeAverage = self.sizeAverage

    -- fold the height and width dims into the mini-batch dim.
    -- print('call nll ')
    self.nllw:updateOutput(self.slsm.output, target, weights)
    self.output = self.nllw.output
    return self.output
end

function SpatialCrossEntropyCriterionWeighted:updateGradInput(input, target, weights)
    assert(input:dim() == 4, 'mini-batch supported only')
    assert(target:dim() == 3, 'mini-batch supported only')
    assert(input:size(1) == target:size(1), 'input and target should be of same size')
    assert(input:size(3) == target:size(2), 'input and target should be of same size')
    assert(input:size(4) == target:size(3), 'input and target should be of same size')
    assert(weights:size(2) == target:size(2), 'weights and target should be of same size')
    assert(weights:size(3) == target:size(3), 'weights and target should be of same size')

    self.nllw:updateGradInput(self.slsm.output, target, weights)

    -- unfold the height and width dims back
    self.slsm:updateGradInput(input, self.nllw.gradInput)
    self.gradInput = self.slsm.gradInput
    return self.gradInput
end

function SpatialCrossEntropyCriterionWeighted:type(type)
    if type then
        self.nllw:type(type)
        self.slsm:type(type)
    end
    parent.type(self, type)
    return self
end

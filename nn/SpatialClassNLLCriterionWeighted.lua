local THNN = require 'nn.THNN'
local SpatialClassNLLCriterionWeighted, parent = torch.class('nn.SpatialClassNLLCriterionWeighted', 'nn.Criterion')

function SpatialClassNLLCriterionWeighted:__init(sizeAverage)
  -- function SpatialClassNLLCriterionWeighted:__init(weights, sizeAverage)
    parent.__init(self)
    if sizeAverage ~= nil then
       self.sizeAverage = sizeAverage
    else
       self.sizeAverage = true
    end
    -- if weights then
    --   --  assert(weights:dim() == 1, "weights input should be 1-D Tensor")
    --    self.weights = weights
    -- end

    self.output_tensor = torch.zeros(1)
    self.total_weight_tensor = torch.ones(1)
    self.target = torch.zeros(1):long()
end

-- function SpatialClassNLLCriterionWeighted:__len()
--    if (self.weights) then
--       return #self.weights
--    else
--       return 0
--    end
-- end

function SpatialClassNLLCriterionWeighted:updateOutput(input, target,weights)
  -- function SpatialClassNLLCriterionWeighted:updateOutput(input, target)
-- target checking
   if type(target) == 'number' then
      if torch.typename(input):find('torch%.Cuda.*Tensor') then
          self.target = torch.CudaLongTensor and self.target:cudaLong() or self.target:cuda()
      else
          self.target = self.target:long()
      end
      self.target[1] = target
   elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
      self.target = torch.CudaLongTensor and target:cudaLong() or target
   else
      self.target = target:long()
   end

-- weights checking
if type(weights) == 'number' then
   if torch.typename(input):find('torch%.Cuda.*Tensor') then
       self.weights = torch.CudaFloatTensor and self.weights:cudaFloat() or self.weights:cuda()
   else
       self.weights = self.weights:cuda()
   end
   self.weights[1] = weights
elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
   self.weights = torch.CudaFloatTensor and weights:cudaFloat() or weights
else
   self.weights = weights:double()
end
assert(weights:size(2)==target:size(2) and weights:size(3)==target:size(3) ," weights tensor should be same size as target tensor")
-- self.weights = weights:double()
-- self.weights = weights
-- print('inside scnllcw.lua..')
   input.THNN.SpatialClassNLLCriterionWeighted_updateOutput(
      input:cdata(),
      self.target:cdata(),
      self.output_tensor:cdata(),
      self.sizeAverage,
      self.weights:cdata(),
      self.total_weight_tensor:cdata()
   )
   self.output = self.output_tensor[1]
   return self.output, self.total_weight_tensor[1]
end

function SpatialClassNLLCriterionWeighted:updateGradInput(input, target, weights)
-- function SpatialClassNLLCriterionWeighted:updateGradInput(input, target)
  -- target checking

   if type(target) == 'number' then
      if torch.typename(input):find('torch%.Cuda.*Tensor') then
          self.target = torch.CudaLongTensor and self.target:cudaLong() or self.target:cuda()
      else
          self.target = self.target:long()
      end
      self.target[1] = target
   elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
      self.target = torch.CudaLongTensor and target:cudaLong() or target
   else
      self.target = target:long()
   end
   -- weights checking
   if type(weights) == 'number' then
      if torch.typename(input):find('torch%.Cuda.*Tensor') then
          self.weights = torch.CudaFloatTensor and self.weights:cudaFloat() or self.weights:cuda()
      else
          self.weights = self.weights:cuda()
      end
      self.weights[1] = weights
   elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
      self.weights = torch.CudaFloatTensor and weights:cudaFloat() or weights
   else
      self.weights = weights:double()
   end

   self.gradInput:resizeAs(input):zero()

   input.THNN.SpatialClassNLLCriterionWeighted_updateGradInput(
      input:cdata(),
      self.target:cdata(),
      self.gradInput:cdata(),
      self.sizeAverage,
      self.weights:cdata(),
      self.total_weight_tensor:cdata()
   )

   return self.gradInput
end

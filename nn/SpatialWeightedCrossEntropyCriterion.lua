local THNN = require 'nn.THNN'
require 'nn.SoftMax'
require 'math'
require 'torch'
local SpatialWeightedCrossEntropyCriterion, parent = torch.class('nn.SpatialWeightedCrossEntropyCriterion', 'nn.Criterion')
--[[
    This criterion does the SpatialCrossEntropyCriterion across
    the feature dimension for a N-channel image of HxW in size.
    It only supports mini-batches (4D input, 3D target)
    It does a LogSoftMax on the input (over the channel dimension),
    so no LogSoftMax is needed in the network at the end
    input = batchSize x nClasses x H x W
    target = batchSize x H x W
    weight = batchSize x H x W

]]--
function SpatialWeightedCrossEntropyCriterion:__init(input, target, weightvalue)
    parent.__init(self)
--     if sizeAverage ~= nil then
--        self.sizeAverage = sizeAverage
--     else
--        self.sizeAverage = true
--     end
--     self.input = input
--     self.target = target
--     self.weight = weight
-- --
--     self.output_tensor = torch.zeros(1)
--     self.total_weight_tensor = torch.ones(1)
--     self.target = torch.zeros(1):long()
end
--
-- function SpatialWeightedCrossEntropyCriterion:__len()
--    if (self.weights) then
--       return #self.weights
--    else
--       return 0
--    end
-- end

function SpatialWeightedCrossEntropyCriterion:updateOutput(input, target, weightvalue)
  if weightvalue then
    assert(input:dim() == 4, "Only 4D input allowed")
    assert(target:dim() == 3, "Only 3D target allowed")
    assert(weightvalue:dim() == 3, "Only 3D weights allowed")
    assert(weightvalue:size(1) == target:size(1), "weight should be same size as target")
    assert(weightvalue:size(2) == target:size(2), "weight should be same size as target")
    assert(weightvalue:size(3) == target:size(3), "weight should be same size as target")
    --  self.weight = weight
  end

  -- print(weightvalue)
  softmaxinput = nn.SoftMax():forward(input)
  self.softmaxinput = softmaxinput
  local output = 0
  for ii = 1, target:size(2) do
    for jj=1, target:size(3) do
      -- if(softmaxinput[1][target[1][ii][jj]][ii][jj]<0) then
        -- print('below zero')
      -- end

      loginput = math.log(softmaxinput[1][target[1][ii][jj]][ii][jj])
      weightedloginput = loginput*weightvalue[1][ii][jj]
      output = output+weightedloginput
      print(output)
    end
  end
  output = output *-1
  output = output/(target:size(2)*target:size(3))

  return output
end
  --  if type(target) == 'number' then
  --     if torch.typename(input):find('torch%.Cuda.*Tensor') then
  --         self.target = torch.CudaLongTensor and self.target:cudaLong() or self.target:cuda()
  --     else
  --         self.target = self.target:long()
  --     end
  --     self.target[1] = target
  --  elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
  --     self.target = torch.CudaLongTensor and target:cudaLong() or target
  --  else
  --     self.target = target:long()
  --  end
   --
  --  input.THNN.SpatialClassNLLCriterion_updateOutput(
  --     input:cdata(),
  --     self.target:cdata(),
  --     self.output_tensor:cdata(),
  --     self.sizeAverage,
  --     THNN.optionalTensor(self.weights),
  --     self.total_weight_tensor:cdata()
  --  )
  --  self.output = self.output_tensor[1]
  --  return self.output, self.total_weight_tensor[1]
-- end

function SpatialWeightedCrossEntropyCriterion:updateGradInput(input, target, weightvalue)

  numClass = input:size(2)
  onehottarget = torch.Tensor()
  onehottarget = onehottarget:resizeAs(input):fill(0)
  onehotweight = torch.Tensor()
  onehotweight = onehotweight:resizeAs(input):fill(0)


  for ii=1, numClass do
    onehottarget[1][ii] = target:eq(ii):double()
    onehotweight[1][ii] = torch.cmul(onehottarget[1][ii], weightvalue)
  end

  self.gradInput:resizeAs(input):zero()
  self.gradInput = self.softmaxinput - onehotweight
   return self.gradInput
end
  --  if type(target) == 'number' then
  --     if torch.typename(input):find('torch%.Cuda.*Tensor') then
  --         self.target = torch.CudaLongTensor and self.target:cudaLong() or self.target:cuda()
  --     else
  --         self.target = self.target:long()
  --     end
  --     self.target[1] = target
  --  elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
  --     self.target = torch.CudaLongTensor and target:cudaLong() or target
  --  else
  --     self.target = target:long()
  --  end
   --
  --  self.gradInput:resizeAs(input):zero()
   --
  --  input.THNN.SpatialClassNLLCriterion_updateGradInput(
  --     input:cdata(),
  --     self.target:cdata(),
  --     self.gradInput:cdata(),
  --     self.sizeAverage,
  --     THNN.optionalTensor(self.weights),
  --     self.total_weight_tensor:cdata()
  --  )
   --

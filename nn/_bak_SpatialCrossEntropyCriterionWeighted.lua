local THNN = require 'nn.THNN'
-- print('before nn call....')
-- require 'nn'
-- print('after nn call....')
local SpatialCrossEntropyCriterionWeighted, parent = torch.class('nn.SpatialCrossEntropyCriterionWeighted', 'nn.Criterion')

function SpatialCrossEntropyCriterionWeighted:__init(sizeAverage)
  -- function SpatialClassNLLCriterionWeighted:__init(weights, sizeAverage)
    parent.__init(self)
    if sizeAverage ~= nil then
       self.sizeAverage = sizeAverage
    else
       self.sizeAverage = true
    end
    -- if weights then
    --   --  assert(weights:dim() == 1, "weights input should be 1-D Tensor")
    --    self.weights = weights
    -- end
    self.slsm = cudnn.SpatialLogSoftMax()

    self.output_tensor = torch.zeros(1)
    self.total_weight_tensor = torch.ones(1)
    self.target = torch.zeros(1):long()
end

-- function SpatialClassNLLCriterionWeighted:__len()
--    if (self.weights) then
--       return #self.weights
--    else
--       return 0
--    end
-- end

function SpatialCrossEntropyCriterionWeighted:updateOutput(input, target,weights)
  -- function SpatialClassNLLCriterionWeighted:updateOutput(input, target)
-- target checking
   if type(target) == 'number' then
      if torch.typename(input):find('torch%.Cuda.*Tensor') then
          self.target = torch.CudaLongTensor and self.target:cudaLong() or self.target:cuda()
      else
          self.target = self.target:long()
      end
      self.target[1] = target
   elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
      self.target = torch.CudaLongTensor and target:cudaLong() or target
   else
      self.target = target:long()
   end

-- weights checking
if type(weights) == 'number' then
   if torch.typename(input):find('torch%.Cuda.*Tensor') then
       self.weights = torch.CudaLongTensor and self.weights:cudaLong() or self.weights:cuda()
   else
       self.weights = self.weights:double()
   end
   self.weights[1] = weights
elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
   self.weights = torch.CudaLongTensor and weights:cudaLong() or weights
else
   self.weights = weights:double()
end
  inputcuda = input:cuda()
  inputslsm = self.slsm:updateOutput(inputcuda)
  input = inputslsm:double()
  self.weights = weights:double()
  self.target = target:long()
  self.output_tensor = self.output_tensor:double()
  self.total_weight_tensor = self.total_weight_tensor:double()
  -- self.sizeAverage = self.sizeAverage:double()
  print('before cnllcriterion ')
   input.THNN.SpatialClassNLLCriterionWeighted_updateOutput(
      input:cdata(),
      self.target:cdata(),
      self.output_tensor:cdata(),
      self.sizeAverage,
      self.weights:cdata(),
      self.total_weight_tensor:cdata()
   )
   self.output = self.output_tensor[1]
   return self.output, self.total_weight_tensor[1]
end

function SpatialCrossEntropyCriterionWeighted:updateGradInput(input, target, weights)
-- function SpatialClassNLLCriterionWeighted:updateGradInput(input, target)
  -- target checking

   if type(target) == 'number' then
      if torch.typename(input):find('torch%.Cuda.*Tensor') then
          self.target = torch.CudaLongTensor and self.target:cudaLong() or self.target:cuda()
      else
          self.target = self.target:long()
      end
      self.target[1] = target
   elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
      self.target = torch.CudaLongTensor and target:cudaLong() or target
   else
      self.target = target:long()
   end
   -- weights checking
   if type(weights) == 'number' then
      if torch.typename(input):find('torch%.Cuda.*Tensor') then
          self.weights = torch.CudaLongTensor and self.weights:cudaLong() or self.weights:cuda()
      else
          self.weights = self.weights:double()
      end
      self.weights[1] = weights
   elseif torch.typename(input):find('torch%.Cuda.*Tensor') then
      self.weights = torch.CudaLongTensor and weights:cudaLong() or weights
   else
      self.weights = weights:double()
   end

   self.gradInput:resizeAs(input):zero()
   self.gradInput = self.gradInput:double()
  --  inputcuda = input:cuda()
  --  inputslsm = self.slsm:updateOutput(inputcuda)
   input = input:double()
   self.weights = weights:double()
   self.target = target:long()
   self.output_tensor = self.output_tensor:double()
   self.total_weight_tensor = self.total_weight_tensor:double()
   print('call updateGradInput ')
   input.THNN.SpatialClassNLLCriterionWeighted_updateGradInput(
      input:cdata(),
      self.target:cdata(),
      self.gradInput:cdata(),
      self.sizeAverage,
      self.weights:cdata(),
      self.total_weight_tensor:cdata()
   )
   gradInputcuda = self.gradInput:cuda()
   inputcuda = input:cuda()
   self.slsm:updateGradInput(inputcuda, gradInputcuda)
   self.gradInput = self.slsm.gradInput
   self.gradInput = self.gradInput:cuda()
   return self.gradInput
end

-- function SpatialCrossEntropyCriterionWeighted:type(type)
--     if type then
--         -- self.nll:type(type)
--         self.slsm:type(type)
--     end
--     parent.type(self, type)
--     return self
-- end
